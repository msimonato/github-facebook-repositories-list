import React from 'react';

const Contributor = (props) => {
  return (
    <div className="col-md-2 mb-4">
      <a href={props.contributorUrl} target="_blank" className="contributor__item">
        <img src={props.contributorAvatar} className="contributor__image" alt="name"/>
        <div className="contributor__name">{props.contributorName}</div>
      </a>
    </div>
  )
};

export default Contributor;