import React from 'react';

import Filter from '../containers/Filter.js'
import RepoList from '../containers/repository/RepoList'

const Sidebar = () => {
  return (
    <aside className="sidebar">
      <section className="sidebar__search">
        <Filter/>
      </section>

      <section className="sidebar__repos">
        <RepoList/>
      </section>
  </aside>
  );
}

export default Sidebar;
