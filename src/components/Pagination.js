import React from 'react';

const Pagination = (props) => {

  const handleClick = (page, index) => {
    props.update({page, index})
  }

  const renderPage = (count) => {
    const result = []
    for (let i = 1; i <= count; i++) {
      result.push(props.paginationUrl.slice(0, -1) + i)
    }
    return result.map((page, i) => {
      return props.activePage !== i
      ?
        <li className="page-item" key={i}><a className="page-link" onClick={() => handleClick(page, i)}>{i + 1}</a></li>
      :
        <li className="page-item active" key={i}><a className="page-link">{i + 1}</a></li>
    })
  }

  const page = renderPage(props.paginationCount)

  return (
    <nav aria-label="pagination">
      <ul className="pagination justify-content-center">
        {page}
      </ul>
    </nav>
  );
}

export default Pagination;