import React from 'react';

const LoadMore = (props) => {

  const handleClick = (url, paginationCount) => {
    props.update({url, paginationCount})
  }

  const load = props.isLoading
    ?
      <button type="button" className="mb-5 btn btn-dark" disabled>Is Loading ...</button>
    :
      <button type="button" className="mb-5 btn btn-dark" onClick={() => handleClick(props.paginationUrl, props.paginationCount)}>Load More Repositories</button>

  return (
    <div className="text-center">
      {load}
    </div>
  )
};

export default LoadMore;