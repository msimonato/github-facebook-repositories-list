import React from 'react';

const repoStat = (props) => {
  return (
    <div className="repo-stat">
      <div className="repo-stat__count">{props.count}</div>
      <div className="repo-stat__label">{props.label}</div>
    </div>
  )
};

export default repoStat;