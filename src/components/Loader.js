import React from 'react';

const Loading = (props) => {
  return (
    <div className="loader">
      <div className="loader__spinner"></div>
    </div>
  )
};

export default Loading;