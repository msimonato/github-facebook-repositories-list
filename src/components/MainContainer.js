import React from 'react';

const MainContainer = (props) => {
  return (
    <main className="main-container">
      <div className="container">
        {props.children}
      </div>
    </main>
  );
}

export default MainContainer;

