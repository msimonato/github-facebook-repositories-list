import React, { Component } from 'react';
import { connect } from 'react-redux';
import { filterRepos } from '../store/action'

class Filter extends Component {

  handleKeyPress = (event) => {
    this.props.filterRepos(event.target.value)
  }

  render() {
    return (
      <div>
        <label htmlFor="search">Filter Repositories</label>
        <input type="search" className="form-control" id="search" placeholder="Filter ..." onKeyUp={this.handleKeyPress}/>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    filterRepos: (search) => dispatch(filterRepos(search))
  }
}


export default connect(null, mapDispatchToProps)(Filter);