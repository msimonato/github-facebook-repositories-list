import React, { Component } from 'react';
import Contributor from '../components/Contributor'
import Loader from '../components/Loader';
import Pagination from '../components/Pagination';
import { connect } from 'react-redux';
import { updateContributors } from '../store/action'

class ContributorList extends Component {
  render() {
    const contributor = this.props.contributors
    ?
      this.props.contributors.map(contributor => {
        return <Contributor
          key={contributor.id}
          contributorName={contributor.login}
          contributorAvatar={contributor.avatar_url}
          contributorUrl={contributor.html_url}/>
      })
    :
      <Loader/>

    const pagination = this.props.paginationCount > 1
    ?
      <Pagination
        paginationStart={{active: 0}}
        paginationCount={this.props.paginationCount}
        paginationUrl={this.props.paginationUrl}
        update={this.props.updateContributors}
        activePage={this.props.activePage}/>
    :
      null

      return (
      <div className="contributor">
        <h5 className="contributor__title mb-4">Contributors list</h5>
        <div className="row mb-5">
          {contributor}
        </div>
        {pagination}
      </div>
    )
  }
};

const mapStateToProps = state => {
  return {
    paginationCount: state.paginationContributorCount,
    paginationUrl: state.paginationContributorUrl,
    activePage: state.paginationActiveContributor
  }
}

const mapDispatchToProps = dispatch => {
  return {
    updateContributors: (contributors) => dispatch(updateContributors(contributors))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ContributorList);