import React, { Component } from 'react';
import eyeIcon from '../../images/eye.svg';
import { connect } from 'react-redux';
import { initRepoInfoContent } from '../../store/action'

class RepoItem extends Component {

  initRepoInfoContent = (repo, index) => {
    this.props.initRepoInfoContent({repo, index})
  }

  render() {
    return (
      <li className='repo__wrap-item' onClick={() => this.initRepoInfoContent(this.props.repo, this.props.selected)}>
        <div className="repo__index">{this.props.selected + 1 }</div>
        <div className="repo__item">
          <h5 className="repo__name">{this.props.repo.name}</h5>
          <div className="repo__watchers">
            <img src={eyeIcon} className="repo__icon" alt="watchers"/>
            <span>{this.props.repo.watchers_count}</span>
          </div>
        </div>
      </li>
    );
  }
}

const mapStateToProps = state => {
  return {
    selectedIndex: state.selectedRepositoryIndex,
    repositories: state.repositories
  }
}

const mapDispatchToProps = dispatch => {
  return {
    initRepoInfoContent: (repo) => dispatch(initRepoInfoContent(repo)),
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(RepoItem);
