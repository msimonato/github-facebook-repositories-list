import React, { Component } from 'react';
import RepoItem from './RepoItem';
import Loader from '../../components/Loader';
import LoadMore from '../../components/LoadMore';
import { connect } from 'react-redux';
import { updateRepos } from '../../store/action'

class RepoList extends Component {

  checkLoadMore = () => {
    if (this.props.paginationCount > 1 && !this.props.search.length) {
      return <LoadMore
      paginationCount={this.props.paginationCount}
      paginationUrl={this.props.paginationUrl}
      isLoading={this.props.loadMoreIsLoading}
      update={this.props.updateRepos}/>
    }
    return null
  }


  render() {

    const content = this.props.repos.length
    ?
      this.props.repos
      // sort for watchers
      .sort(((repoA, repoB) => repoB.watchers_count - repoA.watchers_count))
      // filter repos
      .filter((repo) => repo.name.includes(this.props.search))
      .map( (repo, i) => {
        return <RepoItem key={i} repo={repo} selected={i}/>
      })
    :
      <li><Loader/></li>

    const loadMore = this.checkLoadMore()

    return (
      <div className="repo">
        <h6 className="repo__label">Facebook repository list:</h6>
        <ul className="repo__list mb-5">
          {content}
        </ul>
        {loadMore}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    repos: state.repositories,
    search: state.search,
    paginationCount: state.paginationRepoCount,
    paginationUrl: state.paginationRepoUrl,
    loadMoreIsLoading: state.loadMoreIsLoading,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    updateRepos: (repos) => dispatch(updateRepos(repos))
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(RepoList);