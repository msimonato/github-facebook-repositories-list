import React, { Component } from 'react';
import RepoStat from '../../components/RepoStat'
import ContributorList from '../ContributorList'
import { connect } from 'react-redux';

class RepoInfo extends Component {
  render() {
    return (
      <div>
        <div className="row">
          <div className="col-md-10">
            <h3>{this.props.selectedRepo.name}</h3>
            <p>{this.props.selectedRepo.description}</p>
          </div>
          <div className="col-md-2 text-right">
            <a href={this.props.selectedRepo.html_url} target="_blank" className="btn btn-dark">GitHub page</a>
          </div>
        </div>
        <hr className="mt-5"/>
        <div className="row">
          <div className="col-md-4">
            <RepoStat count={this.props.selectedRepo.watchers_count} label="Watchers"/>
          </div>
          <div className="col-md-4">
            <RepoStat count={this.props.selectedRepo.forks_count} label="forks"/>
          </div>
          <div className="col-md-4">
            <RepoStat count={this.props.selectedRepo.open_issues_count} label="Open issues"/>
          </div>
        </div>
        <hr className="mb-5"/>
        <ContributorList contributors={this.props.contributors}/>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    selectedRepo: state.selectedRepository,
    contributors: state.contributors,
  }
}

export default connect(mapStateToProps)(RepoInfo);
