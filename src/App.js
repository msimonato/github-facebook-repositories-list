import React, { Component } from 'react';

import Sidebar from './components/Sidebar.js'
import MainContainer from './components/MainContainer.js'
import RepoInfo from './containers/repository/RepoInfo.js'
import { connect } from 'react-redux';
class App extends Component {

  render() {
    const content = this.props.selectedRepo
    ?
      <RepoInfo/>
    :
      <div>Click on a Repository to see details</div>

    return (
      <div>
        <Sidebar/>
        <MainContainer>
          {content}
        </MainContainer>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    selectedRepo: state.selectedRepository
  }
}

export default connect(mapStateToProps)(App);
