import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import createSagaMiddleware from 'redux-saga';
import { fetchRepos } from './store/sagas/fetchRepos';
import { watchRepoInfoContent, watchContributors, watchRepositories} from './store/sagas/index';

import App from './App';
import registerServiceWorker from './registerServiceWorker';
import './styles/App.css';
import reducer from './store/reducer';

const sagaMiddleware = createSagaMiddleware();
const store = createStore(reducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),applyMiddleware(sagaMiddleware));

sagaMiddleware.run(fetchRepos);
sagaMiddleware.run(watchRepoInfoContent);
sagaMiddleware.run(watchContributors);
sagaMiddleware.run(watchRepositories);

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));
registerServiceWorker();
