import * as actionType from './actionType'

export const initRepoInfoContent = (repoData) => {
  return ({
    type: actionType.INIT_REPO_INFO_CONTENT,
    repoData: repoData,
  })
}

export const updateContributors = (url) => {
  return ({
    type: actionType.UPDATE_CONTRIBUTORS,
    url: url,
  })
}

export const updateRepos = (data) => {
  return ({
    type: actionType.UPDATE_REPOS,
    data: data,
  })
}

export const fetchRepos = (user) => {
  return ({
    type: actionType.FETCH_REPOS,
    user: user
  })
}

export const setRepos = (repos) => {
  return ({
    type: actionType.SET_REPOS,
    repos: repos
  })
}

export const filterRepos = (search) => {
  return ({
    type: actionType.FILTER_REPOS,
    search: search
  })
}

export const setSelectedRepo = (repo) => {
  return ({
    type: actionType.SET_SELECTED_REPO,
    repo: repo,
  })
}

export const setSelectedIndex = (index) => {
  return ({
    type: actionType.SET_SELECTED_INDEX,
    index: index,
  })
}

export const setContributors = (contributors) => {
  return ({
    type: actionType.SET_CONTRIBUTORS,
    contributors: contributors,
  })
}

export const setPagination = (contributorUrl, pageCount) => {
  return ({
    type: actionType.SET_PAGINATIONS,
    contributorUrl: contributorUrl,
    pageCount: pageCount
  })
}

export const setPaginationRepos = (RepoUrl, pageCount) => {
  return ({
    type: actionType.SET_PAGINATIONS_REPOS,
    RepoUrl: RepoUrl,
    pageCount: pageCount
  })
}

export const setActivePage = (activePage) => {
  return ({
    type: actionType.SET_ACTIVE_PAGE,
    activePage: activePage,
  })
}

export const setIsLoading = (isLoading) => {
  return ({
    type: actionType.LOAD_MORE_IS_LOADING,
    isLoading: isLoading,
  })
}
