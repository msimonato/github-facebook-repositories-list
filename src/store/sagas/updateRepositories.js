import { put } from 'redux-saga/effects';
import axios from 'axios';
import parseHeaders from 'parse-link-header';
import * as actionType from '../actionType'

export function* updateRepositories(action) {
  try {
    yield put({ type: actionType.LOAD_MORE_IS_LOADING, isLoading: true });
    const response = yield axios.get(action.data.url);
    yield put({ type: actionType.SET_REPOS, repos: response.data });
    const parsedHeaders = parseHeaders(response.headers.link)
    if ('next' in parsedHeaders) {
      yield put({ type: actionType.SET_PAGINATIONS_REPOS, RepoUrl: parsedHeaders.next.url, pageCount: parsedHeaders.last.page});
    }
    else {
      yield put({ type: actionType.SET_PAGINATIONS_REPOS, RepoUrl: null, pageCount: 0});
     }
    yield put({ type: actionType.LOAD_MORE_IS_LOADING, isLoading: false });
  } catch (error) {
    console.log("ERROR", error);
  }
}