import { put } from 'redux-saga/effects';
import axios from 'axios';
import parseHeaders from 'parse-link-header';
import * as actionType from '../actionType'

export function* fetchRepos(action) {
  try {
    const response = yield axios.get(
      "https://api.github.com/users/facebook/repos?access_token=28f79e289d11bae7949d58d60d806102c015e96c&per_page=30"
    );
    yield put({ type: actionType.SET_REPOS, repos: response.data });
    const parsedHeaders = yield parseHeaders(response.headers.link) ? parseHeaders(response.headers.link) : {next: { url: null}, last: { page: 1}}
    yield put({ type: actionType.SET_PAGINATIONS_REPOS, RepoUrl: parsedHeaders.next.url, pageCount:  parsedHeaders.last.page});
  } catch (error) {
    console.log("ERROR", error);
  }
}