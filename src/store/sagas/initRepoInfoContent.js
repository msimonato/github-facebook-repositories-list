import { put } from 'redux-saga/effects';
import axios from 'axios';
import parseHeaders from 'parse-link-header';
import * as actionType from '../actionType'

export function* initRepoInfoContent(action) {
  try {
    yield put({ type: actionType.SET_CONTRIBUTORS, contributors: null });
    yield put({ type: actionType.SET_ACTIVE_PAGE, activePage: 0 });
    yield put({ type: actionType.SET_SELECTED_INDEX, index: action.repoData.index });
    yield put({ type: actionType.SET_SELECTED_REPO, repoData: action.repoData.repo });
    const response = yield axios.get(
      `https://api.github.com/repos/facebook/${action.repoData.repo.name}/contributors?access_token=28f79e289d11bae7949d58d60d806102c015e96c&per_page=90`
    );
    yield put({ type: actionType.SET_CONTRIBUTORS, contributors: response.data });
    const parsedHeaders = yield parseHeaders(response.headers.link) ? parseHeaders(response.headers.link) : {last: { url: null, page: 1}}
    yield put({ type: actionType.SET_PAGINATIONS, contributorUrl: parsedHeaders.last.url, pageCount:  parsedHeaders.last.page});
  } catch (error) {
    console.log("ERROR", error);
  }
}