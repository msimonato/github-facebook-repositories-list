import { put } from 'redux-saga/effects';
import axios from 'axios';
import * as actionType from '../actionType'

export function* updateContributors(action) {
  try {
    yield put({ type: actionType.SET_CONTRIBUTORS, contributors: null });
    yield put({ type: actionType.SET_ACTIVE_PAGE, activePage: action.url.index });
    const response = yield axios.get(action.url.page);
    yield put({ type: actionType.SET_CONTRIBUTORS, contributors: response.data });
  } catch (error) {
    console.log("ERROR", error);
  }
}