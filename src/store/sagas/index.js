import { takeEvery } from 'redux-saga/effects'
import { initRepoInfoContent } from './initRepoInfoContent'
import { updateContributors } from './updateContributors'
import { updateRepositories } from './updateRepositories'
import * as actionType from '../actionType'

export function* watchRepoInfoContent(e) {
  yield takeEvery(actionType.INIT_REPO_INFO_CONTENT, initRepoInfoContent)
}

export function* watchContributors(e) {
  yield takeEvery(actionType.UPDATE_CONTRIBUTORS, updateContributors)
}

export function* watchRepositories(e) {
  yield takeEvery(actionType.UPDATE_REPOS, updateRepositories)
}