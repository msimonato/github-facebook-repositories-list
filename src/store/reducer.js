import * as actionType from './actionType'

const initialState = {
  search: '',
  repositories: [],
  selectedRepositoryIndex: null,
  selectedRepository: null,
  contributors: null,
  paginationRepoCount: null,
  paginationRepoUrl: null,
  paginationContributorCount: 0,
  paginationContributorUrl: null,
  paginationActiveContributor: 0,
  loadMoreIsLoading: false
}

const reducer = (state = initialState, action) => {
  switch(action.type) {
    case actionType.SET_REPOS : return {...state, repositories: [...state.repositories, ...action.repos]};
    case actionType.FILTER_REPOS : return {...state, search: action.search}
    case actionType.SET_SELECTED_REPO : return {...state, selectedRepository: action.repoData};
    case actionType.SET_SELECTED_INDEX : return {...state, selectedRepositoryIndex: action.index};
    case actionType.SET_CONTRIBUTORS : return {...state, contributors: action.contributors};
    case actionType.SET_PAGINATIONS : return {...state, paginationContributorUrl: action.contributorUrl, paginationContributorCount: action.pageCount};
    case actionType.SET_PAGINATIONS_REPOS : return {...state, paginationRepoUrl: action.RepoUrl, paginationRepoCount: action.pageCount};
    case actionType.SET_ACTIVE_PAGE : return {...state, paginationActiveContributor: action.activePage};
    case actionType.LOAD_MORE_IS_LOADING : return {...state, loadMoreIsLoading: action.isLoading};
    default : return state;
  }
}
export default reducer